$(document).ready(function() {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gCOLUMN_ORDERID = 0;
  const gCOLUMN_KICHCO = 1;
  const gCOLUMN_LOAIPIZZA = 2;
  const gCOLUMN_NUOC = 3;
  const gCOLUMN_THANHTIEN = 4 ;
  const gCOLUMN_HOTEN = 5;
  const gCOLUMN_SODIENTHOAI = 6;
  const gCOLUMN_TRANGTHAI = 7;
  const gCOLUMN_CHITIET = 8;

  const gORDER_COL = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "chiTiet"];

  var gOrderDb = {
    orders: [],
    // các phương thức để làm việc với dữ liệu users
    filterOrder: function(paramFilterObj) { debugger;
      var vOrderFilterResult = [];
      vOrderFilterResult = this.orders.filter(function(paramOrder) {
        return (
          (paramOrder.loaiPizza.toLowerCase().includes(paramFilterObj.loaiPizza.toLowerCase()) || paramFilterObj.loaiPizza === "") &&
          (paramOrder.trangThai.toLowerCase().includes(paramFilterObj.trangThai.toLowerCase()) || paramFilterObj.trangThai === "")
        );
      });
      return vOrderFilterResult;
    }
  };
  
  // định nghĩa table  - chưa có data
  var gOrderTable = $("#order-table").DataTable( {
    // Khai báo các cột của datatable
    "columns" : [
        { "data" : gORDER_COL[gCOLUMN_ORDERID] },
        { "data" : gORDER_COL[gCOLUMN_KICHCO] },
        { "data" : gORDER_COL[gCOLUMN_LOAIPIZZA] },
        { "data" : gORDER_COL[gCOLUMN_NUOC] },
        { "data" : gORDER_COL[gCOLUMN_THANHTIEN] },
        { "data" : gORDER_COL[gCOLUMN_HOTEN] },
        { "data" : gORDER_COL[gCOLUMN_SODIENTHOAI] },
        { "data" : gORDER_COL[gCOLUMN_TRANGTHAI] },
        { "data" : gORDER_COL[gCOLUMN_CHITIET] }
    ],
    // Ghi đè nội dung của cột action, chuyển thành button chi tiết
    "columnDefs": [ 
    {
        "targets": gCOLUMN_CHITIET,
        "defaultContent": "<button class='info-user btn btn-info order-detail'>Chi tiết</button>"
    }]
  });

  var gId = "";
  var gOrderId="";
  var gObjectRequeestDataOrderConfirm = {
    trangThai: "confirmed"
  };
  var gObjectRequeestDataOrderCancel = {
    trangThai: "cancel"
  };
  
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  
  // // gán click event handler cho button chi tiet
  // $("#order-table").on("click", ".info-user", function() {
    //   onButtonChiTietClick(this); // this là button được ấn
    // });
    
    $("#btn-filter-order").on("click", function() {
      onBtnFilterOrderClick();
    });
    
    $("#order-table").on("click", ".order-detail", function() {
      onBtnDetailOrderClick(this);
    });
    
    $("#order-detail-modal").on("click", "#btn-confirm", function() {
      onBtnConfirmClick(this);
    });
    $("#order-detail-modal").on("click", "#btn-cancel", function() {
      onBtnCancelClick(this);
    });
    
    
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
    onPageLoading();
  
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
  // hàm xử lý sự kiện nút cancel
  function onBtnCancelClick(paramCancelBtn) {
    callApiUpdateOrder(gObjectRequeestDataOrderCancel);
   

  }
  // hàm xử lý sự kiện nút confirm
  function onBtnConfirmClick(paramConfirmBtn) {
    callApiUpdateOrder(gObjectRequeestDataOrderConfirm);
   

  }
   // hàm xử lý sự kiện goi api update order
   function callApiUpdateOrder(paramgObjRequest) {
    
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/"+ gOrderDb.orders.id,
      type: "PUT",
      dataType: 'json',
      contentType:"application/json; charset=utf-8",
      data: JSON.stringify(paramgObjRequest),
      success: function(responseObject){
        //debugger;
       gOrderDb.orders=responseObject;
       $("#order-detail-modal").modal("hide");
       onPageLoading();
        console.log(gOrderDb.orders.id)
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
     
   }

   // hàm xử lý sự kiện nút chi tiet
   function onBtnDetailOrderClick(paramDetailBtn) {
     //lưu thông tin id, orderid vào biến toàn cục
     var vDataRow = getIdFromBtn(paramDetailBtn);
     gId = vDataRow.id;
     gOrderId = vDataRow.orderId;
     console.log(gId , gOrderId)
     $("#order-detail-modal").modal("show");
     // gọi api trả về thông tin order theo orderid
     callApiGetOrderByOrderId();
     
   }
    // hàm gọi api get order by orderid
    function callApiGetOrderByOrderId() { 
      $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders" + "/"+ gOrderId ,
      type: "GET",
      dataType: 'json',
      success: function(responseObject){
        //debugger;
        gOrderDb.orders = responseObject;
        loadDataOrderToTable( gOrderDb.orders);
        console.log(responseObject)
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
  }
   // hàm hiển thị order lên modal table
   function loadDataOrderToTable(gOrderDb) { 
    // console.log(gOrderDb);
    $("#order-id").val(gOrderDb.orderId);
    $("#selected-size").val(gOrderDb.kichCo);
    $("#selected-duongkinh").val(gOrderDb.duongKinh);
    $("#selected-suon").val(gOrderDb.suon);
    $("#selected-salad").val(gOrderDb.salad);
    $("#selected-loaipizza").val(gOrderDb.loaiPizza);
    $("#selected-mavoucher").val(gOrderDb.idVoucher);
    $("#thanhtien").val(gOrderDb.thanhTien);
    $("#phantramgiamgia").val(gOrderDb.giamGia);
    $("#selected-nuoc").val(gOrderDb.idLoaiNuocUong);
    $("#soluong-nuoc").val(gOrderDb.soLuongNuoc);
    $("#input-fullname").val(gOrderDb.hoTen);
    $("#input-email").val(gOrderDb.email);
    $("#input-phone").val(gOrderDb.soDienThoai);
    $("#input-address").val(gOrderDb.diaChi);
    $("#input-msg").val(gOrderDb.loiNhan);
    $("#status").val(gOrderDb.trangThai);
    $("#date-create").val(gOrderDb.ngayTao);
    $("#date-update").val(gOrderDb.ngayCapNhat);
   }
   

   // hàm thu thập dữ liệu id và orderid
   function getIdFromBtn(paramDetaiBtn) { 
    var vTableRow = $(paramDetaiBtn).parents("tr");
    var vOrderRowData = gOrderTable.row(vTableRow).data();
    return vOrderRowData;
    //console.log(vOrderRowData);
     
   }
   // hàm xử lý sự kiện filter users
   function onBtnFilterOrderClick() {
    // Khai báo đối tượng chứa dữ liệu lọc trên form
 
    var vOrderFilterDataObj = {
      loaiPizza: "",
      trangThai: ""
    };
    // B1: Thu thập dữ liệu
    getFilterData(vOrderFilterDataObj);
    // B2: Validate (ko cần)
    // B3: Thực hiện nghiệp vụ lọc
    var vOrderFilterResult = gOrderDb.filterOrder(vOrderFilterDataObj);
    // B4: Hiển thị dữ liệu lên table
    loadDataToTable(vOrderFilterResult);
    console.log(vOrderFilterResult)
  }
  
  // infoFunction sẽ là function các nút cùng gọi
  // function onButtonChiTietClick(paramChiTietButton) {
  //   //Xác định thẻ tr là cha của nút được chọn
  //   var vRowSelected = $(paramChiTietButton).parents('tr');
  //   //Lấy datatable row
  //   var vDatatableRow = gUserTable.row(vRowSelected); 
  //   //Lấy data của dòng 
  //   var vUserData = vDatatableRow.data();      
  //   var vId = vUserData.id;
  //   var vFirstname = vUserData.firstname;  
  //   var vLastname = vUserData.lastname;    
  //   var vCountry = vUserData.country;    
  //   console.log(vId);
  //   console.log(vFirstname);
  //   console.log(vLastname);
  //   console.log(vCountry);
  // }

  // hàm chạy khi trang được load
  function onPageLoading() {
    // lấy data từ server
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
      type: "GET",
      dataType: 'json',
      success: function(responseObject){
        //debugger;
        gOrderDb.orders = responseObject;
        loadDataToTable(responseObject);
        //console.log(gOrderDb)
      },
      error: function(error){
        console.assert(error.responseText);
      }
    });
  }

 
  
  
  // load data to table
  function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gOrderTable.clear();
    //Cập nhật data cho bảng 
    gOrderTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gOrderTable.draw();
  }
   // hàm thu thập dữ liệu lọc trên form
   function getFilterData(paramFilterObj) {
    paramFilterObj.loaiPizza = $("#gPizzaTypeSelect").val().trim();
    paramFilterObj.trangThai = $("#gOrderStatusSelect").val().trim();
  }
});